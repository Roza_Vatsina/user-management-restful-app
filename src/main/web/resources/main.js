jQuery(document).ready(function() {
    let baseUrl = '/usermgmt'

    jQuery('#home-nav-ref').click(function() {
        location.reload();
    });

    if( jQuery('#home').is(':visible')){
        jQuery('#home-nav-ref').addClass('active');
    }
    else if( jQuery('#display').is(':visible')){
        jQuery('#display-nav-ref').addClass('active');
    }
    else if( jQuery('#register').is(':visible')){
        jQuery('#register-nav-ref').addClass('active');
    }

    // jQuery('#display-nav-ref').setAttribute('href').addClass('active');


    // Show list of users
    jQuery('.btn.display-btn').on('click', function (){
        jQuery('#display-nav').addClass('active');

        //Show the proper section
        jQuery('#home').hide();
        jQuery('#register').hide();
        jQuery('#edit').hide();
        jQuery('#display').show();

        jQuery.ajax({
            type: "GET",
            url: baseUrl + '/user/all',
            success: function(response){
                const users = response;
                if(!users || users == null || users.length == 0){
                    jQuery('#no-users').show();
                    jQuery('#users-table').hide();
                }
                else {
                    users.forEach(user  => {
                        let editIcon = document.createElement("i");
                        editIcon.className = "fa fa-edit";
                        let editSpan = document.createElement("span");
                        editSpan.className = "tooltiptext";
                        editSpan.innerText = "Edit User";

                        let editButton = document.createElement("button");
                        editButton.id = "edit-btn-" + user.id;
                        editButton.className = "icon-btn tooltip"
                        editButton.append(editIcon, editSpan);
                        editButton.userId = user.id;
                        editButton.addEventListener("click", function() { showEditUserForm(user.id) }, false);

                        let deleteIcon = document.createElement("i");
                        deleteIcon.className = "fa fa-trash";
                        let deleteSpan = document.createElement("span");
                        deleteSpan.className = "tooltiptext";
                        deleteSpan.innerText = "Delete User";

                        let deleteButton = document.createElement("button");
                        deleteButton.id = "delete-btn-" + user.id;
                        deleteButton.className = "icon-btn tooltip"
                        deleteButton.append(deleteIcon, deleteSpan);
                        deleteButton.userId = user.id;
                        deleteButton.addEventListener("click", function() { deleteUser(user) }, false);

                        let tdName = document.createElement("td");
                        tdName.innerText = user.name;
                        let tdSurname = document.createElement("td");
                        tdSurname.innerText = user.surname;
                        let tdActions = document.createElement("td");
                        tdActions.append(editButton, deleteButton   );

                        let trow = document.createElement("tr");
                        trow.className = 'user-item';
                        trow.append(tdName, tdSurname, tdActions);
                        jQuery('tbody').append(trow);
                    });
                }
            },
            error: function(){
                let msg = 'There was a problem fetching the users. Please try again.';
                jQuery('.msg.error-msg').text(msg).show();
                jQuery('#users-table').hide();
                console.log('There was a problem fetching the users...');
            }
        });
    });

    // Show registration form
    jQuery('.btn.show-registration-btn').on('click', function(){
        jQuery('#display-nav').addClass('active');

        //Show the proper section
        jQuery('#home').hide();
        jQuery('#register').show();
        jQuery('#edit').hide();
        jQuery('#display').hide();

        jQuery('#display-nav').removeClass('active');
        jQuery('#home-nav').removeClass('active');
    });

    // Update user
    jQuery('.btn.update-btn').on('click', function (){
        jQuery('#display-nav').addClass('active');

        //Show the proper section
        jQuery('#home').hide();
        jQuery('#register').hide();
        jQuery('#edit').hide();
        jQuery('#display').show();

        jQuery.ajax({
            type: "POST",
            url: baseUrl + '/user/',
            success: function () {
                let msg = 'The user was updated succesfully!';
                jQuery('.msg.error-msg').text(msg).show();
                jQuery('#edit').hide();
                jQuery('#display').show();
                console.log('The user was updated succesfully.');
            },
            error: function () {
                window.scrollTo({ top: 0, behavior: 'smooth' });
                let msg = 'There was a problem updating the user. Please try again.';
                jQuery('.msg.error-msg').text(msg).show();
                console.log('The user was updated succesfully');
                console.log('There was a problem updating the user.');
            }
        });

    });

    // Add new user
    jQuery('.btn.add-btn').on('click', function (){
        jQuery('#display-nav').addClass('active');

        //Show the proper section
        jQuery('#home').hide();
        jQuery('#register').hide();
        jQuery('#edit').hide();
        jQuery('#display').show();

        let user;
        user.id = 0;
        user.name = jQuery('#new-name').val();
        user.surname = jQuery('#new-surname').val();
        user.gender = jQuery('#new-gender').val();
        user.birthdate = jQuery('#new-birthdate-epoch').val();

        // Work Address
        let address = {};
        address.id = 0;
        address.streeName = jQuery('#new-work-address-street').val();
        address.streetNumber = jQuery('#new-work-address-number').val();
        if (address.streeName == '' && address.streetNumber == ''){
            user.workAddressId = 0;
        }
        else {
            jQuery.ajax({
                type: "POST",
                url: baseUrl + '/address/',
                data: address,
                success: function (response) {
                    user.workAddressId = response.id;
                },
                error: function () {
                    user.workAddressId = 0;
                }
            });
        }

        // Home Address
        address = {};
        address.id = 0;
        jQuery('#edit-form').show();
        address.streeName = jQuery('#new-home-address-street').val();
        address.streetNumber = jQuery('#new-home-address-number').val();
        if (address.streeName == '' && address.streetNumber == ''){
            user.homeAddressId = 0;
        }
        else {
            jQuery.ajax({
                type: "POST",
                url: baseUrl + '/address/',
                data: address,
                success: function (response) {
                    user.homeAddressId = response.id;
                },
                error: function () {
                    user.homeAddressId = 0;
                }
            });
        }

        let dateEpoch = '';
        let date = '';
        if(user.birthdate !== null){
            dateEpoch = user.birthdate;
            date = jQuery.datepicker.formatDate('mm/dd/yy',new Date(dateEpoch * 1000));
        }
        jQuery('#birthdate').val(dateEpoch);
        jQuery('#birthdate-epoch').val(date);




        jQuery.ajax({
            type: "POST",
            url: baseUrl + '/user/',
            success: function () {
                let msg = 'The user was added succesfully!';
                jQuery('.msg.error-msg').text(msg).show();
                jQuery('#edit').hide();
                jQuery('#display').show();
                console.log('The user was added succesfully.');
            },
            error: function () {
                window.scrollTo({ top: 0, behavior: 'smooth' });
                let msg = 'There was a problem updating the user. Please try again.';
                jQuery('.msg.error-msg').text(msg).show();
                console.log('The user was updated succesfully');
                console.log('There was a problem updating the user.');
            }
        });

    });


    function showEditUserForm(userId){
        //Show the proper section
        jQuery('#home').hide();
        jQuery('#register').hide();
        jQuery('#edit').show();
        jQuery('#display').hide();

        jQuery('#display-nav').removeClass('active');
        jQuery('#home-nav').removeClass('active');

        jQuery.ajax({
            type: "GET",
            url: baseUrl + '/user/' + userId,
            success: function(response){
                const user = response;
                if(!user || user == null){
                    jQuery('#no-user').show();
                    jQuery('#edit').hide();
                    jQuery('#display').show();
                }
                else {
                    jQuery.ajax({
                        type: "GET",
                        url: baseUrl + '/address/' + user.workAddressId,
                        success: function(response){
                            const addressStreet = !response.streetName || response.streetName == null ? '' : response.streetName;
                            const addressNumber = !response.strestreetNumberetName || response.streetNumber == null ? '' : response.streetNumber;
                            jQuery('#work-address-street').val(addressStreet);
                            jQuery('#work-address-number').val(addressNumber);
                        }
                    })
                        .then(() => {
                            jQuery.ajax({
                                type: "GET",
                                url: baseUrl + '/address/' + user.homeAddressId,
                                success: function(response){
                                    const addressStreet = !response.streetName || response.streetName == null ? '' : response.streetName;
                                    const addressNumber = !response.strestreetNumberetName || response.streetNumber == null ? '' : response.streetNumber;
                                    jQuery('#home-address-street').val(addressStreet);
                                    jQuery('#home-address-number').val(addressNumber);
                                }
                            });
                    })
                        .then(() => {
                            jQuery('#edit-form').show();
                            jQuery('#name').val(user.name);
                            jQuery('#surname').val(user.username);
                            jQuery('#gender').val(user.gender);

                            let dateEpoch = '';
                            let date = '';
                            if(user.birthdate !== null){
                                dateEpoch = user.birthdate;
                                date = jQuery.datepicker.formatDate('mm/dd/yy',new Date(dateEpoch * 1000));
                            }
                            jQuery('#birthdate').val(dateEpoch);
                            jQuery('#birthdate-epoch').val(date);
                    });
                }
            },
            error: function(){
                let msg = 'There was a problem fetching the user. Please try again.';
                jQuery('.msg.error-msg').text(msg).show();
                jQuery('#users-table').hide();
                console.log('There was a problem fetching the users...');
            }
        });
    };

    function deleteUser(user){
        let confirmDelete = confirm('Are you sure you want to delete ' + user.name + ' ' + user.surname + '?');
        if(confirmDelete) {
            jQuery.ajax({
                type: "DELETE",
                url: baseUrl + '/user/' + user.id,
                success: function(){
                    let msg = ('User ' + user.name + ' ' + user.surname + ' was deleted succesfully!');
                    jQuery('.msg.error-msg').text(msg).show();
                },
                error: function(){
                    console.log('There was a problem deleting the user...');
                    let msg = ('There was a problem deleting user ' + user.name + ' ' + user.surname + '. Please try again.');
                    jQuery('.msg.error-msg').text(msg).show();
                }
            })

        }
    };

    function show(er){
        console.log(er);
    };

});