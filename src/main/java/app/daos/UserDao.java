package app.daos;

import app.model.Address;
import app.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.Transactional;
import java.util.List;

public class UserDao {
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("PERSISTENCE");;
	private EntityManager entityManager;

	public UserDao() {
		entityManager = factory.createEntityManager();
	}

	@Transactional
	public User addUpdateUser(User user) {
		entityManager.getTransaction().begin();
		User mergedUser = entityManager.merge(user);
		entityManager.flush();
		entityManager.getTransaction().commit();
		return mergedUser;
	}

	@Transactional
	public User getUserById(int userId) {
		return entityManager.find(User.class, userId);
	}

	@Transactional
	public List<User> listAllUsers() {
		return (List<User>) entityManager.createNativeQuery(
				"SELECT * FROM Users", User.class)
				.getResultList();
	}

	@Transactional
	public void deleteUser(int userId) {
		User user = entityManager.getReference(User.class, userId);
		Address workAddressId = entityManager.getReference(Address.class, user.getWorkAddressId());
		Address homeAddressId = entityManager.getReference(Address.class, user.getHomeAddressId());
		entityManager.getTransaction().begin();
		entityManager.remove(user);
		entityManager.remove(workAddressId);
		entityManager.remove(homeAddressId);
		entityManager.getTransaction().commit();
	}

}
