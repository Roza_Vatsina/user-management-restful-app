package app.daos;

import app.model.Address;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.Transactional;
import java.util.List;

public class AddressDao {
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("PERSISTENCE");
	private EntityManager entityManager;

	public AddressDao() {
		entityManager = factory.createEntityManager();
	}

	@Transactional
	public Address addUpdateAddress(Address address) {
		entityManager.getTransaction().begin();
		Address mergedAddress = entityManager.merge(address);
		entityManager.flush();
		entityManager.getTransaction().commit();
		return mergedAddress;
	}

	@Transactional
	public Address getAddressById(int addressId) {
		return entityManager.find(Address.class, addressId);
	}

	@Transactional
	public List<Address> listAllAddress() {
		return (List<Address>) entityManager.createNativeQuery(
				"SELECT * FROM Addresses", Address.class)
				.getResultList();
	}

	@Transactional
	public void deleteAddress(int addressId) {
		if (addressId != 0) {
			Address address = entityManager.getReference(Address.class, addressId);
			entityManager.getTransaction().begin();
			entityManager.remove(address);
			entityManager.getTransaction().commit();
		}
	}

}