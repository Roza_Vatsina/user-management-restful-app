package app.resources;

import app.daos.AddressDao;
import app.model.Address;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/address")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AddressResource {
    private static final Logger logger = LoggerFactory.logger(UserResource.class);
    AddressDao addressDao = new AddressDao();

    @POST
    @Path("/")
    @Transactional
    public Address createUpdateAddress(Address address) {
        try {
            return addressDao.addUpdateAddress(address);
        } catch (Exception e) {
            Response response = Response.status(Response.Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON)
                    .entity(address).build();
            throw new BadRequestException("error", response);
        }
    }

    @GET
    @Path("/{id}")
    public Address getAddress(@PathParam("id") int id) {
        try {
            Address address = addressDao.getAddressById(id);
            return address;
        } catch (Exception e) {
            logger.warn("error trying to get address with id: " + id);
            throw new BadRequestException();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response.Status deleteAddress(@PathParam("id") int id) {
        try {
            addressDao.deleteAddress(id);
            return Response.Status.OK;
        } catch (Exception e) {
            logger.warn("error trying to delete address with id: " + id);
            throw new BadRequestException();
        }
    }
}
