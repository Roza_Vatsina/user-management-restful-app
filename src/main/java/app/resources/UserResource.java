package app.resources;

import app.daos.UserDao;
import app.model.User;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/user")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {
    private static final Logger logger = LoggerFactory.logger(UserResource.class);
    UserDao userDao = new UserDao();

    @POST
    @Path("/")
    @Transactional
    public User createUpdateUser(User user) {
        try {
            return userDao.addUpdateUser(user);
        } catch (Exception e) {
            Response response = Response.status(Response.Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON)
                    .entity(user).build();
            throw new BadRequestException("error", response);
        }
    }

    @GET
    @Path("/{id}")
    public User getUserById(@PathParam("id") int id) {
        try {
            User user = userDao.getUserById(id);
            return user;
        } catch (Exception e) {
            logger.warn("error trying to get user with id: " + id);
            throw new BadRequestException();
        }
    }

    @GET
    @Path("/all")
    public List<User> getAllUsers() {
        try {
            return userDao.listAllUsers();
        } catch (Exception e) {
            logger.warn("error trying to get all users");
            throw new BadRequestException();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response.Status deleteUser(@PathParam("id") int id) {
        try {
            userDao.deleteUser(id);
            return Response.Status.OK;
        } catch (Exception e) {
            logger.warn("error trying to delete user with id: " + id);
            throw new BadRequestException();
        }
    }

}
