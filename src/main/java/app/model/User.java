package app.model;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "fname", nullable = false)
    private String name;
    @Column(name = "surname", nullable = false)
    private String surname;
    @Column(name = "gender", nullable = false)
    private int gender;
    @Column(name = "birthdate", nullable = false)
    private int birthdate;
    @Column(name = "home_address_id")
    private int homeAddressId;
    @Column(name = "work_address_id")
    private int workAddressId;

    public User() {
    }

    public User(String name, String surname, int gender, int birthdate, int homeAddressId, int workAddressId) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.birthdate = birthdate;
        this.homeAddressId = homeAddressId;
        this.workAddressId = workAddressId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(int birthdate) {
        this.birthdate = birthdate;
    }

    public int getHomeAddressId() {
        return homeAddressId;
    }

    public void setHomeAddressId(int homeAddressId) {
        this.homeAddressId = homeAddressId;
    }

    public int getWorkAddressId() {
        return workAddressId;
    }

    public void setWorkAddressId(int workAddressId) {
        this.workAddressId = workAddressId;
    }

}
